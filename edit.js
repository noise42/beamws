var express = require('express');
var app = express();
var fs = require("fs");
const child = require('child_process');
var bodyParser = require('body-parser');
var multer  = require('multer');

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));


app.get('/index.html', function (req, res) {
   res.sendFile( __dirname + "/" + "index.html" );
})

app.get('/process_get', function (req,res){
	response = {
		first_name:req.query.first_name,
		last_name:req.query.last_name,
		message:'Hi, '+req.query.first_name+' '+req.query.last_name+'! I was waiting for you'};
	//console.log(response);
	res.end(JSON.stringify(response.message));
	//res.end(resp);
})

app.get('/run', function(req, res){
	var str= 'echo '+req.query.mess+' > test.txt';
	console.log(str);
	var work = child.exec(str, 
		function (error, stdout, stderr){
			if (error){ console.log(error.stack);}
			//console.log('stdout: '+stdout);
			//console.log('stderr: '+stderr);
		});
	work.on('exit', function(code){ console.log('child exited: code '+code);
	});
	res.end('Done!');
})
app.post('/file_upload', function (req, res){
	console.log(req.files.file.name);
	var file  =__dirname+"/"+req.files.file.name;
	fs.readFile( req.files.file.path, function (err, data){
		fs.writeFile(file, data, function(err){
		if (err){
			console.log(err);
		}else{
			response = {
				message:'Success!!',
				filename:req.files.file.name
			};
		}
		console.log(response);
		res.end(JSON.stringify(response));
		});
	});
})

var server = app.listen(8081, function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Listening @ http://%s:%s", host, port)

})
