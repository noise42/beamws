var express = require('express');
var app = express();
var multer  =   require('multer');
var path = require('path');
var fs = require("fs");
var formidable = require('formidable'), util = require('util');
var ip="160.80.35.91";
const child = require('child_process');
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static('public'));

app.get('/home.html', function (req, res) {
   res.sendFile( __dirname + "/" + "home.html" );
})

//Flag controllo
var boolFasta=false;
var boolExample=false;
var boolBackgrn=false;

// Gestione file upload (fasta)
var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './uploads');
  },
  filename: function (req, file, callback) {
	fasta=file.fieldname + '_' + Date.now();
    callback(null, fasta);
  }
});
var upload = multer({ storage : storage}).single('input_file');
var fasta = "", background="";

// Gestione upload backgroud
var storageBack =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './bckgrnd');
  },
  filename: function (req, file, callback) {
	background=file.fieldname + '-' + Date.now()
    callback(null, background);
}
	
});
var uploadBack = multer({ storage : storage}).single('back_file');

//Fasta upload
app.post('/file_upload', function(req, res){
	boolFasta=true;
	//console.log(inputFile);
	upload(req,res,function(err) {
        if(err) {
		console.log(err);
        	return res.end("Error uploading file.");
        }
        res.end("File is uploaded");
	//fasta=req.files.input_file.name;
	//console.log(fasta);
    });
});


//Background
app.post('/back_upload', function(req,res){
	var jsdom = require("jsdom").jsdom;
	var document = jsdom();
	var window = document.defaultView;
	uploadBack(req,res,function(err){
	if(err){
		console.log(err);
		return("Error while uploading file.");
	}
	boolBackgrn=true;
	res.end("File uploaded");
	});
	//var name=req.files.back_file.name;
});


var file_ex="";
//Example button
app.post('/example', function(req, res){
   var fs = require("fs");
   var buf = new Buffer(1024);
   fs.open('./example.fa', 'r+', function(err, fd) {
      if (err) {
         return console.error(err);
      }
      console.log("File opened successfully!");
      console.log("Going to read the file");
      fs.read(fd, buf, 0, buf.length, 0, function(err, bytes){
         if (err){
            console.log(err);
         }
      console.log(bytes + " bytes read");
      // Print only read bytes to avoid junk.
         if(bytes > 0){
            file_ex=(buf.slice(0, bytes).toString());
            //console.log(file_ex);
         }
	boolExample=true;
      });
   });
});

//Start button
app.get('/start', function(req,res){
	var window = req.query.window;
	var motif = req.query.motifs;
	var clean = req.query.clean;
	var fold = req.query.fold;
	var textArea = req.query.sequence;
	res.sendFile( __dirname + "/public/" + "wait.html" );
	//Check flags
	if (boolFasta)
		res.end("You've choose to upload a file. "+ fasta+" is being processed. Please wait...");
	else
		if (boolExample)
			res.end("Example file is being precessed.");
	else{
		res.end(textArea);
		//console.log(textArea,boolFasta,boolExample);
		writeToFile(textArea);
	}
	//Cambiare T > U
	
	if (fold==1){
		var str= 'RNAfold --noPS < '+ +' > RNAfolded.fa';
		//CleanEnergies
		var cmd = './cleanEnergies.py RNAfolded.fa > folded.fa';
		var work = child.exec(str, 
		function (error, stdout, stderr){
			if (error){ console.log(error.stack);}
			//console.log('stdout: '+stdout);
			//console.log('stderr: '+stderr);
		});
		work.on('exit', function(code){ console.log('child exited: code '+code);
	});
	}
	else{
		var str= 'rnastruct.sh ' + + 'folded.fa';
		var work = child.exec(str, 
		function (error, stdout, stderr){
			if (error){ console.log(error.stack);}
			//console.log('stdout: '+stdout);
			//console.log('stderr: '+stderr);
		});
		work.on('exit', function(code){ console.log('child exited: code '+code);
	});
}
	
});

function writeToFile(text){
	var str= 'echo '+text+' > uploads/input_file_'+ Date.now()+'.fa';
	var work = child.exec(str, 
		function (error, stdout, stderr){
			if (error){ console.log(error.stack);}
			//console.log('stdout: '+stdout);
			//console.log('stderr: '+stderr);
		});
	work.on('exit', function(code){ console.log('child exited: code '+code);
	});
}
var server = app.listen(8081, function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Listening @ http://%s:%s", host, port)

})

