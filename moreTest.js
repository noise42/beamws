//Necessario per rendere accessibile delle risorse al pubblico
//basta mettere i files nella cartella <path_to_this_script>/public

var express = require('express'),
    app = express();
//console.log(__dirname);
app.use(express.static(__dirname + '/public'));

app.listen(8080);
